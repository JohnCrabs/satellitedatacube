import os
import snappy

import datetime as dt
import numpy as np

from zipfile import ZipFile
from PIL import Image

# Attributes: Sentinel-2
META_KEY_S2_MISSION_ID: str = "mission_id"
META_KEY_S2_PRODUCT_LEVEL: str = "product_level"
META_KEY_S2_CREATE_DATE: str = "create_date"
META_KEY_S2_CREATE_TIME: str = "create_time"
META_KEY_S2_PROCESSING_BASELINE: str = "processing_baseline"
META_KEY_S2_RELATIVE_ORBIT_NUMBER: str = "relative_orbit_number"
META_KEY_S2_TILE_NUMBER_FIELD: str = "tile_number_field"
META_KEY_S2_SENSING_DATE_STOP: str = "sensing_date_stop"
META_KEY_S2_SENSING_TIME_STOP: str = "sensing_time_stop"
META_KEY_S2_SAFE: str = "safe"

META_KEY_S2_CRS: str = "crs"
META_KEY_S2_EPSG: str = "epsg"
META_KEY_S2_PIXEL_SIZE: str = "pixel_size"
META_KEY_S2_BAND_NAME: str = "band_name"
META_KEY_S2_BAND_RADIOMETRY: str = "band_radiometry"
META_KEY_S2_SPATIAL_RESOLUTION: str = "spatial_resolution"

META_KEY_S2_WIDTH: str = "width"
META_KEY_S2_HEIGHT: str = "height"

S2_MAIN_BANDS = ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B9',
                 'B10', 'B11', 'B12', 'quality_aot', 'quality_wvp',
                 'quality_cloud_confidence', 'quality_snow_confidence',
                 'quality_scene_classification']

S2_DATA_RATIO = {
    'B1': 0.16666666666666666666666666666667,
    'B2': 1.0,
    'B3': 1.0,
    'B4': 1.0,
    'B5': 0.5,
    'B6': 0.5,
    'B7': 0.5,
    'B8': 1.0,
    'B8A': 0.5,
    'B9': 0.16666666666666666666666666666667,
    'B10': 0.16666666666666666666666666666667,
    'B11': 0.5,
    'B12': 0.5,
    'quality_aot': 1.0,
    'quality_wvp': 1.0,
    'quality_cloud_confidence': 0.5,
    'quality_snow_confidence': 0.5,
    'quality_scene_classification': 0.5,
    'view_zenith_mean': 0.5,
    'view_azimuth_mean': 0.5,
    'sun_zenith': 0.5,
    'sun_azimuth': 0.5,
    'view_zenith_B1': 0.16666666666666666666666666666667,
    'view_azimuth_B1': 0.16666666666666666666666666666667,
    'view_zenith_B2': 1.0,
    'view_azimuth_B2': 1.0,
    'view_zenith_B3': 1.0,
    'view_azimuth_B3': 1.0,
    'view_zenith_B4': 1.0,
    'view_azimuth_B4': 1.0,
    'view_zenith_B5': 0.5,
    'view_azimuth_B5': 0.5,
    'view_zenith_B6': 0.5,
    'view_azimuth_B6': 0.5,
    'view_zenith_B7': 0.5,
    'view_azimuth_B7': 0.5,
    'view_zenith_B8': 1.0,
    'view_azimuth_B8': 1.0,
    'view_zenith_B8A': 0.5,
    'view_azimuth_B8A': 0.5,
    'view_zenith_B9': 0.5,
    'view_azimuth_B9': 0.5,
    'view_zenith_B10': 0.16666666666666666666666666666667,
    'view_azimuth_B10': 0.16666666666666666666666666666667,
    'view_zenith_B11': 0.5,
    'view_azimuth_B11': 0.5,
    'view_zenith_B12': 0.5,
    'view_azimuth_B12': 0.5
}

S2_BAND_SPECTRAL = {
    'B1': "Aerosol",
    'B2': "Blue",
    'B3': "Green",
    'B4': "Red",
    'B5': "Vegetation Red Edge",
    'B6': "Vegetation Red Edge",
    'B7': "Vegetation Red Edge",
    'B8': "NIR",
    'B8A': "Vegetation Red Edge",
    'B9': "Water Vapour",
    'B10': "SWIR-Cirrus",
    'B11': "SWIR",
    'B12': "SWIR",
    'quality_aot': "quality_aot",
    'quality_wvp': "quality_wvp",
    'quality_cloud_confidence': "quality_cloud_confidence",
    'quality_snow_confidence': "quality_snow_confidence",
    'quality_scene_classification': "quality_scene_classification"
}

S2_BAND_RESOLUTION = {
    'B1': "60m",
    'B2': "10m",
    'B3': "10m",
    'B4': "10m",
    'B5': "20m",
    'B6': "20m",
    'B7': "20m",
    'B8': "10m",
    'B8A': "20m",
    'B9': "60m",
    'B10': "60m",
    'B11': "20m",
    'B12': "20m",
    'quality_aot': "20m",
    'quality_wvp': "20m",
    'quality_cloud_confidence': "20m",
    'quality_snow_confidence': "20m",
    'quality_scene_classification': "20m"
}

class Sentinel2:
    def __init__(self):
        self.product = None
        self.dataPath = None
        self.folderName = None
        self.metadata = {}
        self.data = {}
        self.latitude = None
        self.longitude = None

    def read(self, dirPath: str, minLat: float=None, maxLat: float=None,
             minLon: float=None, maxLon: float=None,  imgResolution :int=None):
        """
        Read a Copernicus Product
        :param minLat:
        :param maxLat:
        :param minLon:
        :param maxLon:
        :param imgResolution:
        :param dirPath: Path to directory or zip file
        :return: True/False depended on successfully reading or not
        """
        # Error checking
        if not os.path.isdir(dirPath) and not dirPath.__contains__(".zip"):
            print("Error::Unknown data format!")
            return False
        # If passing the error checking
        else:
            self.dataPath = dirPath # Store the path to a tmp variable
            # Check if the path is ZIP format
            if self.dataPath.__contains__(".zip"):
                with ZipFile(self.dataPath) as myZip:
                    self.dataPath = dirPath.split(".zip")[0]  # get the path without the ZIP extension
                    myZip.extractall(self.dataPath)  # unzip the data
            try:
                # Try to read the data (error safety in case of directory is not from Copernicus Hub)
                self.product = snappy.ProductIO.readProduct(self.dataPath)
                self.folderName = os.path.basename(self.dataPath) # Get the directory's name (for metadata)
                print("Info::Product created successfully!")
            except:
                # Exception: Print error message and set the dataPath to None
                print("Error::Directory is not Copernicus Hub format!")
                self.dataPath = None
                return False
            # This point can be accessed only if the process is successful so far
            try:
                self.create_metadata()
                print("Info::Metadata created successfully!")
            except:
                # No need to return at this point, the metadata is not so important as reading the data
                print("Error::Cannot read metadata!")
            try:
                self.create_data(minLat, maxLat, minLon, maxLon, int(imgResolution))
                print("Info::Data created successfully!")
            except:
                # No need to return at this point, the metadata is not so important as reading the data
                print("Error::Cannot create data!")

        return True

    @staticmethod
    def _decode_date(in_date: str):
        year = in_date[:4]
        month = in_date[4:-2]
        day = in_date[-2:]
        return dt.datetime.strptime(f"{year}-{month}-{day}",
                                    "%Y-%m-%d").date()
    @staticmethod
    def _decode_time(in_time: str):
        hour = in_time[:2]
        minutes = in_time[2:-2]
        seconds = in_time[-2:]
        return f"{hour}:{minutes}:{seconds}"

    def _is_product_opened(self):
        errorCondition = (self.product is None or
                          self.dataPath is None or
                          self.folderName is None)
        if errorCondition:
            print("Error::Product has not been read yet. Run \"read\" first.")
            return False
        return True

    def _decode_file_name(self):
        if self.folderName is None or type(self.folderName) is not str:
            print("Error::Product has not been read yet. Run \"read\" first.")
            return False
        decoder = self.folderName.split("_")
        try:
            self.metadata[META_KEY_S2_MISSION_ID] = decoder[0]
            self.metadata[META_KEY_S2_PRODUCT_LEVEL] = decoder[1]
            self.metadata[META_KEY_S2_CREATE_DATE] = self._decode_date(decoder[2].split("T")[0])
            self.metadata[META_KEY_S2_CREATE_TIME] = self._decode_time(decoder[2].split("T")[1])
            self.metadata[META_KEY_S2_PROCESSING_BASELINE] = decoder[3]
            self.metadata[META_KEY_S2_RELATIVE_ORBIT_NUMBER] = decoder[4]
            self.metadata[META_KEY_S2_TILE_NUMBER_FIELD] = decoder[5]
            self.metadata[META_KEY_S2_SENSING_DATE_STOP] = self._decode_date(decoder[6].split(".")[0].split("T")[0])
            self.metadata[META_KEY_S2_SENSING_TIME_STOP] = self._decode_time(decoder[6].split(".")[0].split("T")[1])
            self.metadata[META_KEY_S2_SAFE] = ""
            if decoder[6].split(".").__len__() >= 2:
                self.metadata[META_KEY_S2_SAFE] = "SAFE"
        except:
            print("Error:Something went wrong. Look the folders name if anything wrong.")
            return False
        return True

    def create_metadata(self):
        if not self._is_product_opened():
            return False
        self._decode_file_name()
        bandNames = list(self.product.getBandNames())
        self.metadata[META_KEY_S2_BAND_NAME] = []
        self.metadata[META_KEY_S2_BAND_RADIOMETRY] = []
        self.metadata[META_KEY_S2_SPATIAL_RESOLUTION] = []
        for __band__ in S2_MAIN_BANDS:
            # print(__band__) # Uncomment for debugging
            self.metadata[META_KEY_S2_BAND_NAME].append(__band__)
            self.metadata[META_KEY_S2_BAND_RADIOMETRY].append(S2_BAND_SPECTRAL[__band__])
            self.metadata[META_KEY_S2_SPATIAL_RESOLUTION].append(S2_BAND_RESOLUTION[__band__])
        self.metadata[META_KEY_S2_CRS] = self.product.getSceneCRS().getName().toString()

    @staticmethod
    def _min_max(x, y):
        r_min = min(x, y)
        r_max = max(x, y)
        return r_min, r_max

    @staticmethod
    def _calc_new_pixel_index(px_min: int, px_max: int, mul_res: int,
                              dim_size: int):
        if px_min < 0:
            px_min = 0
        if px_max >= dim_size:
            px_max = dim_size - 1

        dst = abs(px_max - px_min)
        mul = int(round(dst / mul_res))
        if mul == 0:
            mul = 1

        new_px_min = min(px_min, px_max)
        new_px_max = new_px_min + mul * mul_res

        if new_px_max > dim_size:
            new_px_max = dim_size
            if new_px_max - mul * mul_res > 0:
                new_px_min = new_px_max - mul * mul_res
            else:
                new_px_min = new_px_max - (mul - 1) * mul_res

        return new_px_min, new_px_max

    def _Sentinel_calc_WGS84_coords_for_pixel(self, p_x, p_y):
        """
        Get the latitude and longitude value of a pixel.

        :param product: Imagery product opened with snappy
        :param p_x: The pixel in width x-axis
        :param p_y: The pixel in height y-axis
        :return: lat, lon
        """
        if not self._is_product_opened():
            return False

        pos = self.product.getSceneGeoCoding().getGeoPos(snappy.PixelPos(p_x, p_y), None)
        lat = pos.getLat()
        lon = pos.getLon()
        return True, lat, lon

    def _Sentinel_calc_list_of_coordinates(self, product, width:int, height:int, moveX: int=0, moveY: int=0):
        """
        Create a list of coordinates corresponding to each pixel.
        Latitude corresponds to x-axis (width)
        Longitude corresponds to y-axis (height)

        :param product: Imagery product opened with snappy
        :return: lat, lon (numpy array products)
        """
        lat = []
        lon = []
        if width == height:
            for __index__ in range(width):
                p_x = moveX + __index__
                p_y = moveY + __index__
                pos = product.getSceneGeoCoding().getGeoPos(snappy.PixelPos(p_x, p_y), None)
                lat.append(pos.getLat())
                lon.append(pos.getLon())
            lat = np.array(lat)
            lon = np.array(lon)
        else:
            minLat, minLon, maxLat, maxLon = self._calc_WGS84_coordinates(product)
            latStep = (maxLat - minLat) / width
            lonStep = (maxLon - minLon) / height
            lat = np.arange(minLat, maxLat, latStep)
            lon = np.arange(minLon, maxLon, lonStep)

        return lat, lon

    def create_subset(self, px_min_x:int, px_min_y:int, width:int ,height:int):
        # Clip the image
        parameters = snappy.HashMap()
        parameters.put('copyMetadata', True)
        parameters.put('region', "%s, %s, %s, %s" % (px_min_x, px_min_y, width, height))
        subset = snappy.GPF.createProduct("Subset", parameters, self.product)
        return subset

    def create_data(self, minLat: float=None, maxLat: float=None,
                    minLon: float=None, maxLon: float=None, imgResolution :int=None):
        if not self._is_product_opened():
            return False
        bandNames = list(self.product.getBandNames())
        width = self.product.getSceneRasterWidth()
        height = self.product.getSceneRasterHeight()
        _, p_maxLat, p_minLon = self._Sentinel_calc_WGS84_coords_for_pixel(0, 0)
        _, p_minLat, p_maxLon = self._Sentinel_calc_WGS84_coords_for_pixel(width - 1, height - 1)

        # Check if the user has not specified coordinates
        if minLat is None or type(minLat) is not float:
            minLat = p_minLat
        if maxLat is None or type(maxLat) is not float:
            maxLat = p_maxLat
        if minLon is None or type(minLon) is not float:
            minLon = p_minLon
        if maxLon is None or type(maxLon) is not float:
            maxLon = p_maxLon

        minLax, maxLat = self._min_max(minLat, maxLat)
        minLon, maxLon = self._min_max(minLon, maxLon)

        # Find the px_min and px_max values for clipping the image
        gc = self.product.getSceneGeoCoding()
        px_min = gc.getPixelPos(snappy.GeoPos(minLat, minLon), None)
        px_max = gc.getPixelPos(snappy.GeoPos(maxLat, maxLon), None)

        if imgResolution is None or imgResolution <= 0:
            imgResolution = 1

        px_min_x, px_max_x = self._calc_new_pixel_index(int(px_min.x),
                                                        int(px_max.x),
                                                        imgResolution,
                                                        width)
        px_min_y, px_max_y = self._calc_new_pixel_index(int(px_min.y),
                                                        int(px_max.y),
                                                        imgResolution,
                                                        height)

        width = px_max_x - px_min_x
        height = px_max_y - px_min_y

        self.latitude, self.longitude = self._Sentinel_calc_list_of_coordinates(
                                            self.product,
                                            width, height,
                                            px_min_x, px_min_y
                                        )

        self.metadata[META_KEY_S2_WIDTH] = width
        self.metadata[META_KEY_S2_HEIGHT] = height

        for __band__ in S2_MAIN_BANDS:
            self.data[__band__] = np.zeros((width, height))
            if __band__ in bandNames:
                # Due to resolution difference in Sentinel-2 Data calculate the start pixel
                start_px_x = int(px_min_x * S2_DATA_RATIO[__band__])
                start_px_y = int(px_min_y * S2_DATA_RATIO[__band__])

                # Due to resolution difference in Sentinel-2 Data calculate the resolution
                w = int((px_max_x - px_min_x) * S2_DATA_RATIO[__band__])
                h = int((px_max_y - px_min_y) * S2_DATA_RATIO[__band__])

                # Create a temporary px_img to store the image values
                tmp_img = np.zeros(w*h, dtype=np.float32)

                # Read the pixel values
                self.product.getBand(__band__).readPixels(start_px_x, start_px_y, w, h, tmp_img)

                # Reshape the image
                tmp_img = tmp_img.reshape(w, h)

                # Calculate the resolution ratio values
                ratio_w = int(width / w)
                ratio_h = int(height / h)

                # Correct the resolution (scale up the image) and add it to self.data
                tmp_img = np.kron(tmp_img, np.ones((ratio_w, ratio_h)))
                self.data[__band__][0:tmp_img.shape[0], 0:tmp_img.shape[1]] = tmp_img

    def showImage_Gray(self, band_id: int):
        if not self._is_product_opened():
            return False

        bandName = self.metadata[META_KEY_S2_BAND_NAME][band_id]

        print(f"GRAY= {bandName}")

        img = self.data[bandName] # Get image
        # Change the radiometry to uint8
        maxVal = img.max()
        if maxVal == 0:
            maxVal = 1
        img = np.array(img / maxVal * 256, dtype=np.uint8)
        img = Image.fromarray(img)  # Create a PIL image from the array
        img.show()  # Show the Image

        return True

    def showImage_RGB(self, red_id: int, green_id: int, blue_id: int):
        if not self._is_product_opened():
            return False

        if red_id <= 0:
            red_id = 1
        if green_id <= 0:
            green_id = 1
        if blue_id <= 0:
            blue_id = 1

        redName = self.metadata[META_KEY_S2_BAND_NAME][red_id-1]
        greenName = self.metadata[META_KEY_S2_BAND_NAME][green_id-1]
        blueName = self.metadata[META_KEY_S2_BAND_NAME][blue_id-1]

        print(f"RED= {redName}\nGREEN= {greenName}\nBLUE= {blueName}")

        # Get image
        red_img = self.data[redName]
        green_img = self.data[greenName]
        blue_img = self.data[blueName]

        redMax = red_img.max()
        if redMax == 0:
            redMax = 1

        greenMax = green_img.max()
        if greenMax == 0:
            greenMax = 1

        blueMax = blue_img.max()
        if blueMax == 0:
            blueMax = 1

        # Change the radiometry to uint8
        red_img = np.array(red_img / redMax * 256, dtype=np.uint8).T
        green_img = np.array(green_img / greenMax * 256, dtype=np.uint8).T
        blue_img = np.array(blue_img / blueMax * 256, dtype=np.uint8).T

        img = Image.fromarray(np.array([red_img, green_img, blue_img]).T)  # Create a PIL image from the array
        img.show()  # Show the Image

        return True

    def getProduct(self):
        return self.product

    def getMetadata(self):
        return self.metadata

    def getData(self):
        return self.data

    def getLatitudes(self):
        return self.latitude

    def getLongitudes(self):
        return self.longitude

if __name__ == "__main__":
    PATH = "../../Data/Sentinel-2/Piraeus/S2A_MSIL2A_20160201T091212_N0201_R050_T35SKC_20160201T091841.SAFE"
    FILTER_MIN_LAT = 37.921371
    FILTER_MIN_LON = 23.597415
    FILTER_MAX_LAT = 37.963811
    FILTER_MAX_LON = 23.672775
    FILTER_RESOLUTION_MULTIPLIED = 512

    s2 = Sentinel2()
    s2.read(dirPath=PATH,
            minLat=FILTER_MIN_LAT, maxLat=FILTER_MAX_LAT,
            minLon=FILTER_MIN_LON, maxLon=FILTER_MAX_LON,
            imgResolution=FILTER_RESOLUTION_MULTIPLIED)